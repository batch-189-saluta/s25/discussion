// console.log('Hello World')


/*
	JSON OBJECTS
		-JSON stands for Javascript object notation
		JSON is also used in other programming language hence the name javascript object notation.


	Syntax:
		{
			"propertyA" = "valueA"
			"propertyB" = "valueB"
		}

*/

// JSON Object
/*{
	"city": "Quezon City",
	"province": "Metro Manila",
	"country": "Philippines"
}*/


// JSON Array
/*"cities" = [
	{"city": "Quezon City", "province": "Metro Manila", "country": "Philippines"}
	{"city": "Manila", "province": "Metro Manila", "country": "Philippines"}
	{"city": "Makati City", "province": "Metro Manila", "country": "Philippines"}
]*/


// ------------------------------------------------------------------
// JSON Methods
/*
	The JSON objects contains methods for parsing and converting data into a stringified JSON

*/

// ------------------------------------------------------------------
// Convert Data into a stringified JSON

let batchesArr = [
	{batchName: "Batch189"},
	{batchName: "Batch190"}

]

console.log(batchesArr)

// the stringify method is used to convert JS objects into a string
// Before sending data, convert an array or an object to its string equivalent (JSON.stringify())

console.log("Result from a stringified method");
console.log(JSON.stringify(batchesArr));


let data = JSON.stringify({
	name:"John",
	age: 31,
	address: {
		city: "Manila",
		country: "Philippines"
	}
})

console.log(data)

// User details example -----------------------------------------

/*let firstName = prompt("What is you first name?");
let lastName = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live in?"),
	country: prompt("Which country does your city address belong to?")
}

let otherData = JSON.stringify({
	firstName: firstName,
	lastName: lastName,
	age: age,
	address: address
})
console.log(otherData)*/


// --------------------------------------------------------------
// COnvert JSON into JS object
/*
		JSON.parse()
*/

let batchesJSON = `[
	{
		"batchName": "Batch189"
	},
	{
		"batchName": "Batch190"
	}
]`
console.log(batchesJSON)

// Upon recieveing data, the JSON text can be converted to JS object so that we can use it t our program
console.log("Result from parse method")
console.log(JSON.parse(batchesJSON))

let stringifiedObject = `{
		"name": "Ivy",
		"age": "18",
		"address": {
			"city": "Caloocan City",
			"country":"Philippines"
		}
}`
console.log(stringifiedObject)
console.log(JSON.parse(stringifiedObject))

